---
title: "Japanese"
date: 2022-08-22 23:21:00
tags: ["japanese"]
---
I first picked up Japanese as something to do for fun while in high school, though exactly when I do not remember. By
the time I got to college, I was taking it seriously, but not seriously enough to dedicate more than just a couple hours
per week to studying. That needed to change. By the time my freshman year was over, I had one Japanese language course
under my belt, and knew there were at least 3 more ahead of me. The classroom setting is hit-or-miss for learning
languages (and whether it hits or misses is effected by so many factors that each day of class is a new gamble). At the
very least, taking a course gave me something I desperately needed: a whip. I was never going to study if I didn't have
to, but with a grade on the line and a perfectionist reputation to defend, I had just the right ingredients to force me
to learn.

I'll be upfront: I have gotten rusty. Very, very rusty. I have forgotten how to read many kanji, how to write many more,
and easily 100 vocabulary words. I've also forgotten a number of useful grammar tools, like ておく, but I am wholely
confident that I can recover most of those losses and return to my studies even stronger. Expect blogs ;)
