---
title: "Rust's async sucks"
date: 2023-02-12 16:20:00
tags: ["programming"]
---
*Note: this post was revised the day after its original publication in response to CraftSpider's valued input.*

I've wanted to write a "Rust isn't the perfect unicorn you think it is" post for a while, but instead of one big fat
dunk, I'm going to point to my one major issue with the language. Perhaps in future posts I'll mention the brick wall
learning curve, terrible name, borrow checker hell, et cetera. But for now,

## Async is a joke

Let's face it, Rust's async is just horrible. Of course, there is a good reason for that: it isn't done yet. Too bad
that isn't an excuse in my book. The use of futures is something I feel very iffy about altogether, but then again C#
uses futures to great effect. Shall we go over that?

In C#, a future is a simple thing: you call an async function that returns type `T`, and you get a `Task<T>` which
will eventually become a `T` without any warning. This quiet change in type is, I suspect, not something the Rust devs
would be too psyched about. Right now, calling an `async fn() -> T` in Rust yields you an `impl`. Not a type, not even
a trait, an *implementation* statement. C#'s distinction between `Task<T>` and `T` is clear: one is a future which will
eventually contain data of the desired type, and the other is said data. But Rust's `impl` return is very odd and leads
to clunky type declarations like this:

```rs
let x: impl Future<Output = i32> = foo("abc");
```

What does that even mean? How am I supposed to read that? What actually *is* that type, and what can it do? Well,
certainly one thing it can do is `await`, so let's do that now.

```rs
let y: i32 = x.await?;
```

Much better. Credit where it's due, Rust's `?` operator is quite nice (where you can use it, at least) and using it on
`await` is no exception. Since we're awaiting anyways, we might as well throw that all in one line. Here it is in Rust:

```rs
let z: i32 = foo("abc").await?;
```

And for completeness, C#:

```cs
int z = await foo("abc");
```

First thing to notice: C#'s `await` is an operator, and therefore a keyword. In fact, Rust's is also a keyword, but for
some reason that keyword behaves like a function call. Except it doesn't have parens. So, Rust, what is `await`: is it
a function or not?

C#'s `await` operator is actually just syntactic sugar. Because futures have a known type (`Task`), they can also have
known methods. Every future in C# has the `Task.Wait` method, where the actual await functionality is implemented. The
`await` operator makes that easier to read. While Rust's `await` keyword rides the line between operator and function,
C#'s knows what it is: an operator, used to more easily read code that is clearly implemented in a method. Is it any
wonder that C# is the heavyweight world champion of parallelization? Apparently, Rust's `await` keyword was subject to
months of arguing about whether it would be prefix notation (as in C#) or postfix notation (which they ultimately went
with). I will touch on this more later, but a part of the conundrum is that `await` in Rust is actually syntactic sugar
for some generator{{< super "[2]" >}} magic.

C# also provides tools for parallelization that Rust just won't. The best example I can think of is
`System.Threading.Tasks.Parallel` which provides `For` and `ForEach` functions to easily make parallel loops. Even
better, the thread's integer ID and the loop's overall state are accessible within the loop body, empowering the
programmer to manipulate a thread easily, kill the loop prematurely, and so on. For instance:

```cs
using System.Threading.Tasks;
Parallel.For(0, 12, (i, loop) => {
	//This region, of type `Action`, is just a closure.
	//`i` is a thread ID of type `int` and `loop` contains the state of the loop.
	if(i % 3 == 0) {
		Console.WriteLine("This is thread {0}, and its ID is a multiple of 3", i);
	} else if(i == 10) {
		loop.Stop(); //Stop doing anything as soon as thread ID 10 is processed
	}
});
```

Rust, meanwhile, is a jerk, and doesn't give you any of this. "But wait!", shout the rustaceans, "you can use Rayon for
that kind of thing, and Tokio for anything heavier."  
Let's take a walk.

First of all, I should not need an external library for something that's just "kinda nice". C's stdlib doesn't even have
anything analogous to Rust's `Vec`, let alone `HashMap` or `async`. But you know what? C's stdlib is old. C itself is
ancient. Nobody expects grandpa to have an extensive standard library. Rust, on the contrary, is actively trying to
replace C, to become the next all-encompassing systems programming language. As such, its libstd{{< super "[1]" >}} has
these common tools. Remember: `std` ≠ `core`. Nobody expects par-for in `core`, but it really, really ought to be in
`std`. And another thing: most async in Rust demands Tokio, the big, all-powerful parallelization crate for Rust. The
fact that I must employ a nuke to win a fistfight is really, REALLY frustrating. I'll touch on this again in the next
section.

## Synchronous async

Sometimes, library authors will see that their functions can be labeled as asynchronous, empowering users to operate
concurrently. This is a good thing, but in Rust, it's also a problem. Our case study shall be
[obws](https://docs.rs/obws/0.10.0/obws/), an OBS WebSocket wrapper.

I wrote a simple program for myself to save from OBS's replay buffer. Here's the code:

```rs
use obws::{Client, Result};

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<()> {

	//Connect to OBS
	let mut client = Client::connect(
		"localhost",
		4455u16,
		Some("no way i put my password in a blog post lmfao")
	).await?;

	client.replay_buffer().save().await?;

	//Leave
	client.disconnect().await;

	Ok(())
}
```

Notice anything? All the `obws` functions I call are `async`, but I don't actually want to do anything asynchronously
with them. For this, I need to have `async fn main()`, *which is a compile-time error!* In order to have an asynchronous
`main`, you need to bring in Tokio with its `rt` and `macros` features enabled. You need to use the super-powered,
massive async library to send a couple packets to a websocket.

With all disrespect Rust, **why the fuck did you think that was a good idea?** What I wrote above is almost identical to
the docs' example `obws` code which simply prints its version. Printing a library's version requires Tokio.
*Printing a library's version requires another library.*

Let's – ugh, I need a drink – let's compare to C# again. Firstly, C# 7.1 added `async Main` right into the language, no
cheating necessary. Second, along with `async Main`, two more output types for `Main` were made available: `Task` and
`Task<int>`. These can be synchronous or async, as can the classic `int` and `void` outputs. As long as you have
`async Main`, C is happy to let you `await` function calls in `Main`. The important part is: in C#, you can trivially
use an async library synchronously because you can have `async Main` without any external fuss.

## How can Rust's async be made good

There are three problems that need to be solved. Lets tackle them in order:

1. **Change the return type of an async function call.**

Instead of a trait, `Future<T>` should be a type in and of itself. This simplifies the chain of types, drops the future
data after awaiting, clearly differentiates a future from the resultant type, maintains expectations of ownership, and
ensures `let x: T = foo.bar().await?` works as expected. This would remove the ability for async library developers to
write their own `.poll` implementation, but this is easily solved with a `Poller` trait such as this one:

```rs
pub trait Poller {
	type Output;
	fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output>;
}
```

If this looks like the current `Future` trait, that's because it almost is. Noteably, `.await` (as alluded to earlier)
wraps the call to `poll` which, as it stands, already needs to be manually implemented by async libraries. Practically,
this change is entirely one of presentation, not of engineering. In my opinion, for a language that prides itself on
clarity and flexibility, presentation matters just as much as the engineering within, so I'm not averse to changes like
this.

2. Leveraging the generator magic mentioned earlier, **find good syntax for `await`.**

The current syntax treats compile-time sugar like a struct field, changing it to `.await()` would treat sugar like a
function call, and prefix notation makes the `?` operator fugly. In my ideal world, `await` would be a `fn(self) -> T`
defined in the `Poller` trait, but I have been advised that this approach leads to a compiler design nightmare. I spent
all of like 6 milliseconds thinking about a syntax to suggest, but I can't think of any winners.

3. **Make `async fn main()` a core language feature.**

The fact that this isn't already standard is just sad. Allowing something like `async fn main() -> Future<ExitCode>`
would be cool, but isn't strictly necessary.

My 1st proposal does have a few drawbacks, however. First, it is a breaking change in the type system; according to
semver, it could not be implemented until Rust 2.0 (pft, like that'll ever be released). Second, adding a type that does
nothing without manually implementing a trait that already exists is a bit "smoke and mirrors"-y by Rust standards.

Regardless, there you have it. Rust's async sucks, but it doesn't have to.

---

{{< super "[1]" >}}It should be called stdlib. Almost every language calls it that. What is Rust doing? I'm not too keen
on "trait" either; it's really a type class.  
{{< super "[2]" >}}Yeah, a stable feature desugars to nightly-only code. Clown language.
