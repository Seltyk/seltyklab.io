---
title: "Posts"
date: 2022-08-22
---
This is where I ramble about things that don't matter and present opinions you'll probably disagree with. Unlike Drew
DeVault, my takes aren't backed up by years of hardcore industry experience, and they certainly won't spark any
interesting discussion. Neverthenonetheless, I shall leave them here for the world to stare at bewilderedly.
