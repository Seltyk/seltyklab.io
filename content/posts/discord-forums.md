---
title: "Stop making Discord your community hub!"
date: 2023-01-01 16:32:00
tags: ["misc"]
---
*Update 2023-02-13: added an excellent real-world example of my point*

Discord is not a replacement for old-school forums. I am sick and fucking tired of finding out that the entirety of an
online community is centered on a Discord server.  
Rushdown Revolt, Yakuza modding, NDS-era Pokémon modding, I'm calling you all out.

## The gist of it

Thriving communities provide a means for new people to join and a means for them to stay. That means supporting the
three basic needs of an online community:

1. Newcomers need an approachable place to begin their investment
2. Members who are invested need a way to delve deeper into the community's knowledgebase and history
3. Veterans who have since left the community need to archive and preserve their knowledge

Forums provide all 3 of these needs via web crawlers and topic organization. Discord, by contrast, *prevents* meeting
these needs through walled gardens, intimidation, and the infinite scroll of a channel.

## Where do newcomers begin?

Before someone can join a community, they have to find out it exists, then get a feel for what it's all about. We'll
ignore that second part for now because these days people seek out communities for things they already understand at a
base level. Nobody will join a Minecraft community if they're never played, for example. That said, when a community has
an upfront source of basic info (e.g. "this is how you install Quilt mods for Minecraft"), newcomers will have an easier
time navigating things they never knew or didn't fully understand in advance. In any case, let's focus on problem zero:
how does someone find a community? The answer is as simple as you think it is. They find it on Google. Chances are, most
people reading this will have found the Minecraft Forum or Project Pokémon by just typing something about Minecraft or
Pokémon modding into Google. Newcomers can also find communities via links elsewhere on the web. If memory serves (as it
so often doesn't), I found Minecraft Forum because every mod review on YouTube linked the mod's download page in the
description. You wanna guess what site that link always led to?

Communities can also link to each other. I'm making up hypotheticals here, but as an example, I wouldn't be surprised if
there was a community somewhere online dedicated to creating wooden furniture, and a separate community which focuses
specifically on making a business selling wooden furniture from your garage. No surprise, these communities would be
linking each other back and forth all day. Someone in the former wants to learn to sell their creations? They go to the
latter. Someone from the latter is confused how another user's countersunk screw holes look so clean? They go to the
former.

Lastly, but perhaps most importantly, people who have found a community for the first time need a way to get started.
They need a comfortable sandbox of sorts where they can take in information slowly and avoid being overwhelmed. It's
crucial that community leaders be mindful of shy and timid newcomers; they especially will want to explore the community
before engaging with it. In as few words as possible: enable lurkers.

### In the blue corner: the forum

Forums are websites, so the Google and linking problems are both solved automatically. I don't think I need to explain
myself here. If I do, Google it; I'm sure there's a community willing to set you straight.

Lurkers are a part of forum culture. These Curious Georges of the internet can read along and feel out a community until
they're comfortable enough to engage. In the long run, this leads to more active members and a tighter-knit community.

### In the red corner: the Discord server

Discord servers cannot be found on Google or any other web crawler. Heck, they can't even be found on *Discord*. There
are "community hubs" now which ease this pain, but they don't actually solve the core issue, because users are left to
wonder "is this other server going to be more helpful to me?". At any rate, you'll likely find users of other sites
talking about a Discord server and hopefully providing an invite link, but this adds a layer to a cake which must be
very very thin: explorer, crawler, source. The Discord server community approach adds a 4th layer, between the crawler
and source, reducing the number of explorers willing to find a source of information. Additionally, while Discord *does*
feature a search bar (and a surprisingly competent one at that), the results it collates only serve to reinforce the
scattered nature of a Discord community's knowledgebase – more on that later.

"Hang on," the unscrupulous of you may be thinking, "I for one am pretty annoyed at having to explain the same basics to
newbies all the time. Reducing the amount of incoming community members eases the teaching process for veterans and
ensures only those who really care will stick around." To you I have but two comments. First of all, you're gatekeeping;
fuck you and fuck off. Second of all, I have an entire section below about preserving knowledge which should address
this line of thinking, so keep your pants on. Spoiler alert: it's an L for Discord.

Discord has the linking problem roughly half-solved; a half-assed solution from a half-assed JavaScript app. Every
version of Discord neatly embeds a "join" button to invite links, and provides a one-click method to generate invite
links to a specific channel in a server on-the-fly. This is honestly great! It means communities can connect easily and
explorers who've lost their way can be sent back to civilization with no effort. Unfortunately, linking to *content* in
another server (or, frankly, in the same server) is nigh impossible. We'll touch more on linking in Discord a few
sections down.

## How do people dig in?

Say you've just joined a new community. You've spent some time getting comfortable, you recognize a few frequent faces,
and you've even begun to engage with other community members. You feel welcome, and importantly, you feel motivated to
learn more. You, o' brave one, want just one thing: a fast way to comb through subtopics of interest to you. So where do
you start?

### From the east: the forum

Forums tend to have simple yet effective search bars, but if they don't appending the forum's name to a web search will
suffice. But here's the magic: you don't actually have to do that. Forums are organized into subforums which can recurse
for a bit until you get to threads, the bread and butter of the forum. If you have a vague idea what you're looking for,
you need only follow the subforum topics, and if you're lost, there's probably a pinned thread to help guide you.

If you don't find the info you're looking for, you can just ask for it. Find an appropriate subforum, start a thread,
and provide details. You probably won't get an answer right away, maybe not even for the rest of the day, but herein
lies the genius of the forum: new threads are typically displayed on the front page. Within a day or so, someone with
the wisdom you seek *will* find your thread and *will* give you answers. Additionally, because forum discussion is
asynchronous and achieved through massive posts, you will get a very complete answer. If an admin thinks your thread
belongs in another subforum, they'll simply move it to that subforum. The point is: ask, and ye shall receive.

To be fair, some forums are better about organization than others, and when a forum declines, it becomes a lot harder to
to get recent information. As a small exercise, I spent about 5 minutes looking through Minecraft Forum for info on the
recently-released Quilt, but came up empty-handed. Forums can wither for many reasons, but by far the saddest cause of
death is the community migrating to Discord. Why would they do that, and why is it such a bad idea?

### From the west: the Discord server

Discord offers a simple organization scheme: one channel per topic, and related channels are grouped together. This
system, combined with the nature of real-time conversation, is very appealing to the most active forum members. They
like being able to talk to other active users with zero delay, and because they're already comfortable navigating lots
of topics, they don't find Discord's organization cumbersome. These users tend to play the role of community leaders in
any online community, whether they realize it or not, so the more time they spend on Discord, the faster other community
members migrate with them.

Before you even go looking for information, you've got a few hurdles to climb over. At best, you have to steel the
oddly-intimidating nature of the infinite scrolling channel, where several people are having several parallel
conversations about god knows what, and sentences wizz by faster than you can read them. At worst, you also have to read
an incommodious list of rules, give yourself a bunch of optional roles, post an introduction paragraph to a channel no
one reads (and nobody, *nobody* likes writing those), and scroll to the top of #announcements to find some basic info on
the server.

Now that you're settled in, trying to find info in a Discord server is, respectfully, **way too hard**. The search bar
only finds keywords, and because human conversations aren't formal, one-topic affairs, you'll hit dozens if not hundreds
of false positives. You can jump to these messages (if the UI lets you) and scroll back a bit for some context, and you
might actually find what you were looking for, but don't count on it. If you're especially unlucky, your search could be
interrupted by an admin pinging @everyone with some unsightly essay that you now have to read, or the admins forgot to
prohibit users pinging @everyone so a troll or a bot will fire that in every channel. Hope your speakers aren't on!

Eventually you get sick of this sisyphean word search and resort to just asking people in a channel that you think is
relevant. Three fates await you, which I have sorted in order of likelihood.

1. You get ignored. Maybe nobody sees your message amidst the constant influx of drivel, maybe everyone in that channel
   is preoccupied with their own conversations, maybe the people online at the moment aren't too keen on talking to
   noobs like you, or maybe they've been asked this question 1000 times and just don't care anymore. One way or another,
   you're getting ignored.
2. You get scolded for posting in the wrong channel. Discord mods/admins have a reputation for iron-fisted ruling, and
   mercy toward the new guy is, shall we say, unexpected. Hopefully they point you to the right channel this time, but
   if they don't, you can't guess wrong again or you risk getting banned.
3. You get an incomplete answer. The kind soul gifting you with the knowledge of the ancients knows how quickly your
   message will scroll away if they let it, so they rush out an answer. It's helpful, to be sure, but it also doesn't
   get you very far. To any who join the Rushdown Revolt Discord server, this is what your life will be like as you try
   to learn the game and provide feedback to the developers (who are very cool, by the way, and graciously accept
   criticism).

I briefly mentioned a problem with linking to content on Discord, and now is a good place to address that. Basically,
you can copy the link to any individual message on Discord, even across channels or servers, and send that to someone
else. In other words, just like linking a specific forum message or Stack Exchange answer, you can link a specific
Discord message to someone in need. The problem is, you have to find that message, and your recipient needs access to
the channel housing it. If they're not in the server you linked, or don't have permission to read the channel, then your
link is useless to them. While this problem *can* exist on forums, it is very rare, and demonstrates that a forum admin
is unusually (unhelpfully (unlikeably)) lax toward forum culture.

## How do the old guard archive their vast wisdom?

Wan Shi Tong buried his library, so communities have to preserve their knowledge independently. This is how pioneering
research into game engines spreads, this is how beginner tutorials reach the minds of newcomers, and this is how
documentation is passed from the old generation to the new. Good news, forums do this automatically. Bad news, Discord
servers are incapable of it.

### Your hero: the forum

When people stop posting in a forum thread, nothing happens. It just sits there in perpetuity, waiting to be discovered
by a wearily traveler in the future. The [DenverCoder9 problem](https://xkcd.com/979/) exists specifically because
forums preserve old threads, but it's honestly a good problem to have – not only does it mean you aren't the first
person to experience an issue, it means there is at least one site whose users are likely to have an answer. After all,
DenverCoder9 wouldn't have posted there if they didn't anticipate a response.

This kind of posterity lends forum users the tools to archive their knowledge.

- Put a tutorial on YouTube, make a forum post, and title it accordingly. Bam, knowledge preserved.
- Do some research, compile your findings into a forum post, and make a thread in a relevant subforum. Bam.
- Find some cool bug or technique in a game, describe the repro steps (bonus points for video evidence). Bam.

Anyone and everyone, as mentioned previously, can find this information again later. Additionally, other forum users can
augment it. If you're wondering what others think or how they're further explored the topic, you need only scroll down
and keep reading. Even if that topic is several years old, you could jump in with your own thoughts and
discoveries.{{< super "[1]" >}}

### The dastardly villain: the Discord server

After about 5 minutes, maybe 10, whatever someone said is long gone, flushed away by the endless onslaught of new
messages. Maybe it can be found again, maybe not, but you'll be hard-pressed to find any subsequent discussion. There is
no posterity, just a gigabyte of jargon. If you're lucky, the server admins thought ahead and mitigated this problem.
But "one often meets his destiny on the path he takes to avoid it", and boy do they ever meet their destinies.

There are essentially two ways to counter the posterity problem, and both only cause new problems.

1. Granular channel topics. Specific channel topics make information easier to find and ensure that conversation about
   one specific post lasts as long as possible. Unfortunately, this means you need a lot of channels, which – regardless
   of how you *try* to prevent it ­– will always raise the barrier to entry by further intimidating newcomers.
2. Informational channels. Users who have a reputation for documenting information can be permitted to post in channels
   dedicated to hosting helpful information. This approach gives every member a place to read without enabling
   flushers. The downside, however, is that people who aren't experts can still provide useful information, and this
   approach silences their voices. Sure, they could provide feedback in other channels, but that forces flushing.

## Conclusion

I am a proponent of the freedom of knowledge, and one cannot understate the difference between a forum and a Discord
server on this freedom. Forums allow anyone with access to Google to find information about something, so long as they
have a keyword and a few minutes of their time. Discord's search functionality requires having access to the server in
the first place, then demands much patience as you scroll through four simultaneous conversations in #general, only one
of which is pertinent to your query. Forums create freedom of knowledge, while Discord stifles it.

This isn't Discord's fault, mind you. Discord is meant to be a place of real-time conversation, not of asynchronous
discussion, and I differentiate the words "conversation" and "discussion" intentionally. Discord is very good at
providing a place where people can talk about Whatever, but it's very bad at providing a place for people to deliberate
about one defined topic. That is the common thread throughout this post: using Discord to host a community is simply
using the wrong tool for a noble job. Hammers for nails, drivers for screws; don't just use what you're comfortable
with, use what's *best*.  
Use ***forums***.

---

## Addendum: link aggregators

Reddit and similar sites like the [Lemmy-verse](https://join-lemmy.org/), called link aggregators, are very similar to
forums. In terms of intent, link aggregators are used to share links to other pages. In terms of technical design, link
aggregators can be thought of as superforums. Instead of the whole site being dedicated to one topic broken up into
subforums, each topic belongs to a subreddit (or whatever Lemmy calls 'em). Additionally, while link aggregators don't
support subforums, they do support subthreading, wherein each comment on a thread can become a thread in and of itself.

If you've used Reddit at any point, you may have noticed how many communities avoid link aggregation altogether. Entire
subreddits are dedicated to conversation or original content. Not surprisingly, this can make link aggregators rather
effective as Discord substitutes, as they avoid the common bickering that made Discord replace forums.

- "Everybody already has a Discord account, why ask them to make another?"
  + Everybody also has a Reddit account.
- "Discord servers are so easy. One click, and it's ready to go."
  + Subreddits are so easy. One click, and it's ready to go.
- "Discord provides powerful tools for moderation."
  + No, not really, not compared to Reddit. Though I will admit, Reddit *alone* isn't very good for moderation, but
    browser add-ons like Mod Toolbox get the job done well.
- "Discord is as cross-platform as it gets!"
  + Do you even know what Reddit is?

So if you're too lazy to operate an actual forum, just make a subreddit. It's a far cry better than Discord.

## Addendum: a real-world example

Toyli reached out to me with a link to the
[Helix discussion board on GitHub](https://github.com/helix-editor/helix/discussions). Imagine if this were a Discord
server. Imagine the innate chaos in communication here, where posts just come and go and even a forum channel is
inadequate for controlling the conversation. Imagine trying to search through all this valuable input with Discord's
search bar. Imagine if you couldn't read any of these comments without making a Discord account, joining the server, and
agreeing to the rules with an emote reaction. Now that GitHub has discussion boards built-in (which as you can tell
**are modeled after traditional forums**), there is little reason for a software project's community hub to be on
Discord or Reddit.

Helix's discussion board is a perfect case study, an example of everything I said in this post being done right.

---

{{< super "[1]" >}}This is called necrobumping. Usually it's considered n00b behavior or trolling, because it moves
archaic threads to the top of the page, but there are legitimate reasons to necrobump.
