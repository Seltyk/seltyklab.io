---
title: "About"
---
I'm Seltyk (or CSeltyk if the name was taken already lol), a programmer from the USA. While I have several languages
under my belt, to varying degrees of understanding, my best work is made in C, Lua, and Rust. My hobbies include gaming,
studying linguistics, and digging through Wikipedia articles at 4 in the morning. I wish I had real world hobbies but
unfortunately I live in car-dependent hellhole America, where there is naught but asphalt as far as the eye can see;
there isn't much to do here. Sooner or later I intend to move to Europe and escape this nightmare.

- Occasionally I stream games on Twitch, just for funsies.

- If you'd like to send me something with end-to-end encryption (good idea), please use
  [this public key](https://gist.githubusercontent.com/Seltyk/89d02f6bd502f3142f52d7433a6f0270/raw/3b985d3d6b3d3d3c5adbf03b8f991d564802339d/pubkey.gpg)
  and send to the associated address.

- I would talk about work or education and whatnot, but this is not a username I intend to use for personal affairs.

- 3、4年間ぐらい日本語を学んだことあるけど、少しだけわかります。まだ勉強してますよ！

---
